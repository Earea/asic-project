---
title: "ASIC - Get ASX Listed Companies"
author: "Bruce, Dean, Adrian @ Bond University"
output: pdf_document
---

# Download the Official Listed Company List from ASX then save as a csv file.
  
* Get ASX Listed Companies is used to refresh the official list from the ASX.

This only needs to be re-run when the official company list needs updating.  Some basic standardizing is done, such as:  

* trim whitespace
* change LTD to Limited
* remove [..]

```{r eval=TRUE}

###########################
## INITILIZATION ##########
###########################

suppressWarnings(library(XML,quietly=TRUE))
suppressWarnings(library(RCurl,quietly=TRUE))
suppressWarnings(library(stringi,quietly=TRUE))
suppressWarnings(library(stringr,quietly=TRUE))
suppressWarnings(library(qdapRegex,quietly=TRUE))

dataDir <- 'H:\\Documents\\asic-project\\Data\\'
#dataDir <- "X:\\_Research\\Simone - ASIC Enforcements\\Repo\\Data\\"

###########################
## LOAD REQUIRED DATA #####
###########################

fileurl <- "http://www.asx.com.au/asx/research/ASXListedCompanies.csv"
ASXdata <- read.table(fileurl, sep = ',' , skip = 3)

###########################
## STANDARDIZE DATA #######
###########################

ASXdata[,1] <- str_trim(ASXdata[,1],side="both") 
ASXdata[,1] <- paste(ASXdata[,1]," ",sep='')
ASXdata[,1] <- paste(" ",ASXdata[,1],sep='')

ASXdata[,1] <- gsub("[Ll][Tt][Dd], ","Limited, ", ASXdata[,1])
ASXdata[,1] <- gsub("[Ll][Tt][Dd]. ","Limited. ", ASXdata[,1])
ASXdata[,1] <- gsub("[Ll][Tt][Dd] ","Limited ", ASXdata[,1])
ASXdata[,1] <- gsub("[Ll][Tt][Dd].$","Limited.", ASXdata[,1])
ASXdata[,1] <- gsub("[Ll][Tt][Dd]$","Limited", ASXdata[,1])
ASXdata[,1] <- gsub("LIMITED, ","Limited, ", ASXdata[,1]) 
ASXdata[,1] <- gsub("LIMITED. ","Limited. ", ASXdata[,1]) 
ASXdata[,1] <- gsub("LIMITED ","Limited ", ASXdata[,1]) 
ASXdata[,1] <- gsub("LIMITED.$","Limited.", ASXdata[,1]) 
ASXdata[,1] <- gsub("LIMITED$","Limited", ASXdata[,1]) 
ASXdata[,1] <- gsub("limited, ","Limited, ", ASXdata[,1]) 
ASXdata[,1] <- gsub("limited. ","Limited. ", ASXdata[,1]) 
ASXdata[,1] <- gsub("limited ","Limited ", ASXdata[,1]) 
ASXdata[,1] <- gsub("limited.$","Limited.", ASXdata[,1]) 
ASXdata[,1] <- gsub("limited$","Limited", ASXdata[,1])

ASXdata[,1] <- gsub("\\|","", ASXdata[,1]) 
ASXdata[,1] <- gsub("\\{","\\(", ASXdata[,1]) 
ASXdata[,1] <- gsub("\\}","\\)", ASXdata[,1]) 
ASXdata[,1] <- gsub("\\(.*?\\)","", ASXdata[,1])
ASXdata[,1] <- gsub("\\(","", ASXdata[,1]) 
ASXdata[,1] <- gsub("\\)","", ASXdata[,1]) 
ASXdata[,1] <- gsub(" +"," ", ASXdata[,1])
ASXdata[,1] <- gsub("[..]","", ASXdata[,1])

colnames(ASXdata) <- c("CompanyName","Ticker","Sector")
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
ASXdata$CompanyName <- trim(ASXdata$CompanyName)
ASXdata$Ticker <- trim(ASXdata$Ticker)
ASXdata$Sector <- trim(ASXdata$Sector)
ASXdata$TitleCases <- TC(ASXdata$CompanyName)
ASXdata$TitleCases <- as.character(ASXdata$TitleCases)

###########################
## SAVE OUTPUT ############
###########################

write.csv(ASXdata,paste(dataDir,"ASXListedCompanies.csv",sep=''),row.names = FALSE)
```

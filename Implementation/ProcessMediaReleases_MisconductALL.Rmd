---
title: "ASIC - Produce Timeline Table for Identified Misconduct"
author: "Bruce, Dean, Adrian @ Bond University"
output: pdf_document
---

```{r eval=TRUE}

###########################
## INITILIZATION ##########
###########################

library(knitr)
library(stringr)
library(plyr)

dataDir <- 'H:\\Documents\\asic-project\\Data\\'
#dataDir <- 'E:\\Documents\\asic-project\\Data\\'
#dataDir <- "X:\\_Research\\Simone - ASIC Enforcements\\Repo\\Data\\"

###########################
## LOAD REQUIRED DATA #####
###########################

misconductList <- read.csv(paste(dataDir,"MisconductClassifications4ALL.csv",sep=''),stringsAsFactors = FALSE)
ASICdf <- read.csv(paste(dataDir,"MediaReleases_PROCESSED_NLP.csv",sep=''),stringsAsFactors = FALSE)
ASICdf$Misconduct <- NA
ASICdf$Breaches <- NA

###########################
## CREATE FUNCTIONS #######
###########################

detectMisconduct <- function(content,misconductList) {
  detectMisconduct <- "Unknown"
  for (i in 1:nrow(misconductList))
    if ((regexpr(misconductList[i,1],
                 content,perl = TRUE)[1]) > -1) detectMisconduct <- c(detectMisconduct,misconductList[i,2])
  if (length(detectMisconduct) > 1) detectMisconduct <- detectMisconduct[-1]
  detectMisconduct
}

###########################
## DETECT MISCONDUCT ######
###########################

ASICdf <- ASICdf[!is.na(ASICdf$Content),] #Remove rows where content is NA

#Detect Misconduct conducted
for (i in 1:nrow(ASICdf)) {
  if (!is.na(ASICdf$Content[i])) 
  {
    ASICdf$Misconduct[i] <- paste(detectMisconduct(ASICdf$Content[i],misconductList),collapse = ",")
    ASICdf$Breaches[i] <- length(strsplit(x = ASICdf$Misconduct[i],split = ",")[[1]])
    if (ASICdf$Misconduct[i] == "Unknown") ASICdf$Breaches[i] <- NA
  }
}

###########################
## SAVE OUTPUT ############
###########################

write.csv(ASICdf,paste(dataDir,"MediaReleases_PROCESSED_NLP_MISCONDUCTALL.csv",sep=""),row.names = FALSE)
```

---
title: "ASIC - Detect Entity Functions"
author: "Bruce, Dean, Adrian @ Bond University"
output: pdf_document
---

This file contains two seperate functions which detect business entities inside of the content provided. One method utilizes NLP to find entities, while the other method searches through the content provided and identified companies which appear in a user specified csv file.

```{r eval=TRUE}

###########################
## INITILIZATION ##########
###########################

dataDir <- 'H:\\Documents\\asic-project\\Data\\'
#dataDir <- 'E:\\Documents\\asic-project\\Data\\'
#dataDir <- "X:\\_Research\\Simone - ASIC Enforcements\\Repo\\Data\\"

ASICdf <- read.csv(paste(dataDir,"MediaReleases_RAW.csv",sep=''),stringsAsFactors = FALSE)
```


# NLP Method
```{r eval=TRUE}

###########################
## INITILIZATION ##########
###########################

library(coreNLP)
library(plyr)

nlpDir <- "H:\\Documents\\stanford-corenlp-full-2015-04-20\\stanford-corenlp-full-2015-04-20"
#nlpDir <- "E:\\Documents\\stanford-corenlp-full-2015-04-20"

coreFile <- "stanford-corenlp-full-2015-04-20"
initCoreNLP(nlpDir,mem="4g")

#Create list of user specified organizations to be excluded from NLP results
Remove <- c("EU", "ASIC","Australian Securities and Investments Commission","ASX","Asx Limited","Asx Limited Incorporated")
Remove2 <- c(" -RRB- "," -LRB- ")
RemovePattern <- paste0("\\b(", paste0(Remove, collapse="|"), ")\\b")
RemovePattern2 <- paste0("\\b(", paste0(Remove2, collapse="|"), ")\\b")

###########################
## CREATE FUNCTIONS #######
###########################

annotateNLP <- function(content) {
  contentDf <- data.frame(content)
  #Standardise content to improve consistency
  contentDf[,1] <- gsub("[Ll][Tt][Dd], ","Limited, ", contentDf[,1])
  contentDf[,1] <- gsub("[Ll][Tt][Dd]. ","Limited. ", contentDf[,1])
  contentDf[,1] <- gsub("[Ll][Tt][Dd] ","Limited ", contentDf[,1])
  contentDf[,1] <- gsub("[Ll][Tt][Dd].$","Limited.", contentDf[,1])
  contentDf[,1] <- gsub("[Ll][Tt][Dd]$","Limited", contentDf[,1])
  contentDf[,1] <- gsub("LIMITED, ","Limited, ", contentDf[,1]) 
  contentDf[,1] <- gsub("LIMITED. ","Limited. ", contentDf[,1]) 
  contentDf[,1] <- gsub("LIMITED ","Limited ", contentDf[,1]) 
  contentDf[,1] <- gsub("LIMITED.$","Limited.", contentDf[,1]) 
  contentDf[,1] <- gsub("LIMITED$","Limited", contentDf[,1]) 
  contentDf[,1] <- gsub("limited, ","Limited, ", contentDf[,1]) 
  contentDf[,1] <- gsub("limited. ","Limited. ", contentDf[,1]) 
  contentDf[,1] <- gsub("limited ","Limited ", contentDf[,1]) 
  contentDf[,1] <- gsub("limited.$","Limited.", contentDf[,1]) 
  contentDf[,1] <- gsub("limited$","Limited", contentDf[,1])
  
  contentDf[,1] <- gsub("[Pp][Tt][Yy], ","Proprietary, ", contentDf[,1])
  contentDf[,1] <- gsub("[Pp][Tt][Yy]. ","Proprietary. ", contentDf[,1]) 
  contentDf[,1] <- gsub("[Pp][Tt][Yy] ","Proprietary ", contentDf[,1]) 
  contentDf[,1] <- gsub("[Pp][Tt][Yy].$","Proprietary.", contentDf[,1]) 
  contentDf[,1] <- gsub("[Pp][Tt][Yy]$","Proprietary", contentDf[,1]) 
  contentDf[,1] <- gsub("\\(Pty.?), ","Proprietary, ", contentDf[,1])
  contentDf[,1] <- gsub("\\(Pty.?). ","Proprietary. ", contentDf[,1])
  contentDf[,1] <- gsub("\\(Pty.?) ","Proprietary ", contentDf[,1])
  contentDf[,1] <- gsub("\\(Pty.?).$","Proprietary.", contentDf[,1])
  contentDf[,1] <- gsub("\\(Pty.?)$","Proprietary", contentDf[,1])
  
  contentDf[,1] <- gsub("INC, ","LegalCorp, ", contentDf[,1])
  contentDf[,1] <- gsub("INC. ","LegalCorp. ", contentDf[,1])
  contentDf[,1] <- gsub("INC ","LegalCorp ", contentDf[,1])
  contentDf[,1] <- gsub("INC.$","LegalCorp.", contentDf[,1])
  contentDf[,1] <- gsub("INC$","LegalCorp", contentDf[,1])
  contentDf[,1] <- gsub("Inc, ","LegalCorp, ", contentDf[,1])
  contentDf[,1] <- gsub("Inc. ","LegalCorp. ", contentDf[,1])
  contentDf[,1] <- gsub("Inc ","LegalCorp ", contentDf[,1])
  contentDf[,1] <- gsub("Inc.$","LegalCorp.", contentDf[,1])
  contentDf[,1] <- gsub("Inc$","LegalCorp", contentDf[,1])
  contentDf[,1] <- gsub("Inc., ","LegalCorp, ", contentDf[,1])
  contentDf[,1] <- gsub("Inc.. ","LegalCorp. ", contentDf[,1])
  contentDf[,1] <- gsub("Inc. ","LegalCorp ", contentDf[,1])
  contentDf[,1] <- gsub("Inc..$","LegalCorp. ", contentDf[,1])
  contentDf[,1] <- gsub("Inc.$","LegalCorp", contentDf[,1])
  contentDf[,1] <- gsub("LegalCorp, ","Incorporated, ", contentDf[,1])
  contentDf[,1] <- gsub("LegalCorp. ","Incorporated. ", contentDf[,1])
  contentDf[,1] <- gsub("LegalCorp ","Incorporated ", contentDf[,1])
  contentDf[,1] <- gsub("LegalCorp.$","Incorporated.", contentDf[,1])
  contentDf[,1] <- gsub("LegalCorp$","Incorporated", contentDf[,1])
  
  contentDf[,1] <- gsub("Incorporated, ","Inc , ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated. ","Inc . ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated ","Inc  ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated.$","Inc .", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated$","Inc .", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary. Limited, ","Proprietary Limited, ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary. Limited. ","Proprietary Limited. ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary. Limited ","Proprietary Limited ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary. Limited.$","Proprietary Limited.", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary. Limited$","Proprietary Limited", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary, Limited, ","Proprietary Limited, ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary, Limited. ","Proprietary Limited. ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary, Limited ","Proprietary Limited ", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary, Limited.$","Proprietary Limited.", contentDf[,1])
  contentDf[,1] <- gsub("Proprietary, Limited$","Proprietary Limited", contentDf[,1])
  contentDf[,1] <- gsub("Limited, ","Limited Incorporated, ", contentDf[,1])
  contentDf[,1] <- gsub("Limited. ","Limited Incorporated. ", contentDf[,1])
  contentDf[,1] <- gsub("Limited ","Limited Incorporated ", contentDf[,1])
  contentDf[,1] <- gsub("Limited.$","Limited Incorporated.", contentDf[,1])
  contentDf[,1] <- gsub("Limited$","Limited Incorporated", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated[.|,]? Incorporated, ","Incorporated, ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated[.|,]? Incorporated. ","Incorporated. ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated[.|,]? Incorporated ","Incorporated ", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated[.|,]? Incorporated.$","Incorporated.", contentDf[,1])
  contentDf[,1] <- gsub("Incorporated[.|,]? Incorporated$","Incorporated", contentDf[,1])
  
  contentDf[,1] <- gsub("\\{","\\(", contentDf[,1]) 
  contentDf[,1] <- gsub("\\}","\\)", contentDf[,1]) 
  contentDf[,1] <- gsub("\\(.*?\\)","", contentDf[,1])
  contentDf[,1] <- gsub("\\(","", contentDf[,1]) 
  contentDf[,1] <- gsub("\\)","", contentDf[,1]) 
  contentDf[,1] <- gsub(" +"," ", contentDf[,1])
  colnames(contentDf)[1] <- "content"
  
  #ContentDf <- contentDf$content
  #ContentDf
  aContentDf <- annotateString(contentDf$content)
  aContentDf
}

detectEntityNLP <- function(content) {
  contentDf <- data.frame(NA)
  colnames(contentDf)[1] <- "content"
  contentDf$Ltd <- NA
  contentDf$Inc <- NA
  contentDf$Pty <- NA
  contentDf$Organizations <- NA
  tokenized <- getToken(content)
    
  NER_Org <- tokenized[tokenized$NER=="ORGANIZATION",]
  NER_Org$GroupToken <- as.numeric(NER_Org$id) - seq(1, length(NER_Org$id))
  NER_Org$Common <- NER_Org$sentence*NER_Org$GroupToken
  Identify <- ddply(NER_Org, .(Common), summarise, GroupIdentified=paste0(token, collapse=" "))
  Unique <- unique(Identify$GroupIdentified)
  Unique <- gsub(Unique,pattern = "Proprietary Limited",replacement = 'Pty')
  UniqueAll <- paste(Unique,collapse = ",")
  Ref_Limited <- grep(pattern = "Limited",x = Unique,value = TRUE)
  Ref_Inc <- grep(pattern = "Inc ",x = Unique,value = TRUE)
  Ref_Pty <- grep(pattern = "Pty",x = Unique,value = TRUE)
  contentDf$Ltd[1] <- paste(Ref_Limited,collapse = ",")
  contentDf$Inc[1] <- paste(Ref_Inc,collapse = ",")
  contentDf$Pty[1] <- paste(Ref_Pty,collapse = ",")
  contentDf$Pty <- gsub("Pty","Proprietary Limited",contentDf$Pty)
  contentDf$Organizations[1] <- UniqueAll
    
  uniqueLtd <- NA
  contentDf$Ltd[1] <- paste(
    c(uniqueLtd,matrix(unlist(unique(strsplit(x = contentDf$Ltd,split = ",")[1])))),collapse = ",")
  contentDf$Ltd <- gsub("NA,?","",contentDf$Ltd)
  contentDf$Ltd <- gsub(RemovePattern,"",contentDf$Ltd)
  contentDf$Ltd <- gsub(RemovePattern2,"",contentDf$Ltd)
  fixed <- NA
  
  if (length(strsplit(contentDf$Ltd[1],split=",")[[1]]) > 1) 
  {
    for (j in 1:length(strsplit(contentDf$Ltd[1],split=",")[[1]]))
    { 
      strsplit(contentDf$Ltd[1],split=",")[[1]][j]
      fixed <-c(fixed,gsub("(.Limited).*","\\1",strsplit(contentDf$Ltd[1],split=",")[[1]][j])) 
    } 
  } #Remove everything after Limited
  
  if (length(strsplit(contentDf$Ltd[1],split=",")[[1]]) == 1) 
  {
    strsplit(contentDf$Ltd[1],split=",")[[1]][1]
    fixed <- gsub("(.Limited).*","\\1",strsplit(contentDf$Ltd[1],split=",")[[1]][1]) 
  } 
  
  contentDf$Ltd[1] <- paste(unique(fixed),collapse = ",")
  contentDf$Ltd <- gsub("NA,?","",contentDf$Ltd)
  contentDf$Ltd <- gsub("^Limited$","",contentDf$Ltd)
  contentDf$Ltd <- gsub("^ ","",contentDf$Ltd)
  contentDf$Ltd <- gsub(", ",",",contentDf$Ltd)
  contentDf$Ltd <- paste(unique(strsplit(contentDf$Ltd,split = ",")[[1]]),collapse = ",")
  contentDf$Ltd <- gsub("^Limited Incorporated$","",contentDf$Ltd)
  contentDf$Ltd <- gsub("^Limited Incorporated,","",contentDf$Ltd)
  contentDf$Ltd <- gsub(",Limited Incorporated,",",",contentDf$Ltd)
  contentDf$Ltd <- gsub(",Limited Incorporated$","",contentDf$Ltd)
  
  uniquePty <- NA
  contentDf$Pty[1] <- paste(
    c(uniquePty,matrix(unlist(unique(strsplit(x = contentDf$Pty,split = ",")[1])))),collapse = ",")
  contentDf$Pty <- gsub("NA,?","",contentDf$Pty)
  contentDf$Pty <- gsub(RemovePattern,"",contentDf$Pty)
  contentDf$Pty <- gsub(RemovePattern2,"",contentDf$Pty)
  fixed <- NA
  
  if (length(strsplit(contentDf$Pty[1],split=",")[[1]]) > 1) 
  {
    for (j in 1:length(strsplit(contentDf$Pty[1],split=",")[[1]]))
    { 
      strsplit(contentDf$Pty[1],split=",")[[1]][j]
      fixed <-c(fixed,gsub("(.Proprietary Limited).*","\\1",strsplit(contentDf$Pty[1],split=",")[[1]][j])) 
    } 
  }
  
  if (length(strsplit(contentDf$Pty[1],split=",")[[1]]) == 1) 
  {
    strsplit(contentDf$Pty[1],split=",")[[1]][1]
    fixed <- gsub("(.Proprietary Limited).*","\\1",strsplit(contentDf$Pty[1],split=",")[[1]][1]) 
  } 
  
  contentDf$Pty[1] <- paste(unique(fixed),collapse = ",")
  contentDf$Pty <- gsub("NA,?","",contentDf$Pty)
  contentDf$Pty <- gsub("^Proprietary Limited$","",contentDf$Pty)
  contentDf$Pty <- gsub("^ ","",contentDf$Pty)
  contentDf$Pty <- gsub(", ",",",contentDf$Pty)
  contentDf$Pty <- paste(unique(strsplit(contentDf$Pty,split = ",")[[1]]),collapse = ",")
  contentDf$Pty <- gsub("^Proprietary Limited$","",contentDf$Pty)
  contentDf$Pty <- gsub("^Proprietary Limited [Incorporated]?,","",contentDf$Pty)
  contentDf$Pty <- gsub(",Proprietary Limited [Incorporated]?,",",",contentDf$Pty)
  contentDf$Pty <- gsub(",Proprietary Limited [Incorporated]?$","",contentDf$Pty)
  
  uniqueInc <- NA
  contentDf$Inc[1] <- paste(
    c(uniqueInc,matrix(unlist(unique(strsplit(x = contentDf$Inc,split = ",")[1])))),collapse = ",")
  contentDf$Inc <- gsub("NA,?","",contentDf$Inc)
  contentDf$Inc <- gsub(RemovePattern,"",contentDf$Inc)
  contentDf$Inc <- gsub(RemovePattern2,"",contentDf$Inc)
  contentDf$Inc <- gsub("Inc.","Inc",contentDf$Inc)
  fixed <- NA
  
  if (length(strsplit(contentDf$Inc[1],split=",")[[1]]) > 1) 
  {
    for (j in 1:length(strsplit(contentDf$Inc[1],split=",")[[1]]))
    { 
      strsplit(contentDf$Inc[1],split=",")[[1]][j]
      fixed <-c(fixed,gsub("(.Inc).*","\\1",strsplit(contentDf$Inc[1],split=",")[[1]][j])) 
    } 
  }
  
  if (length(strsplit(contentDf$Inc[1],split=",")[[1]]) == 1) 
  {
    strsplit(contentDf$Inc[1],split=",")[[1]][1]
    fixed <- gsub("(.Inc).*","\\1",strsplit(contentDf$Inc[1],split=",")[[1]][1]) 
  } 
  
  contentDf$Inc[1] <- paste(unique(fixed),collapse = ",")
  contentDf$Inc <- gsub("NA,?","",contentDf$Inc)
  contentDf$Inc <- gsub("^Inc$","",contentDf$Inc)
  contentDf$Inc <- gsub("^ ","",contentDf$Inc)
  contentDf$Inc <- gsub(", ",",",contentDf$Inc)
  contentDf$Inc <- paste(unique(strsplit(contentDf$Inc,split = ",")[[1]]),collapse = ",")
  
  if (contentDf$Ltd[1] == "") {contentDf$Ltd[1] <- NA}
  if (contentDf$Inc[1] == "") {contentDf$Inc[1] <- NA}
  if (contentDf$Pty[1] == "") {contentDf$Pty[1] <- NA}
  if (contentDf$Organizations[1] == "") {contentDf$Organizations[1] <- NA}
  
  contentDf <- subset(contentDf,select = -content)
  contentDf
}

```

# Basic Method
```{r eval=TRUE}
library(plyr)

detectEntityKnown <- function(content) {
  #Load tables containing known entities
  ASXdata <- read.csv(paste(
    dataDir,"ASXListedCompanies.csv",sep=''),sep = ',',stringsAsFactors = FALSE)
  SubsidiariesData <- read.csv(paste(
    dataDir,"SubsidiariesTable_PROCESSED.csv",sep=''),sep = ',',stringsAsFactors = FALSE)

  #Clean content
  contentDf <- data.frame(content)
  contentDf[,1] <- gsub("[Ll][Tt][Dd].?","Limited", contentDf[,1])
  contentDf[,1] <- gsub("LIMITED","Limited", contentDf[,1]) 
  contentDf[,1] <- gsub("limited","Limited", contentDf[,1]) 
  contentDf[,1] <- gsub("[Pp][Tt][Yy].? Limited","Proprietary Limited", contentDf[,1])
  contentDf[,1] <- gsub("\\(Pty.) Limited","Proprietary Limited", contentDf[,1]) 
  contentDf[,1] <- gsub("\\(Pty) Limited","Proprietary Limited", contentDf[,1]) 
  contentDf[,1] <- gsub("INC.","LegalCorp", contentDf[,1]) 
  contentDf[,1] <- gsub("INC ","LegalCorp", contentDf[,1]) 
  contentDf[,1] <- gsub("Inc.","LegalCorp", contentDf[,1]) 
  contentDf[,1] <- gsub("Inc ","LegalCorp", contentDf[,1]) 
  contentDf[,1] <- gsub("LegalCorp","Incorporated", contentDf[,1]) 
  contentDf[,1] <- gsub("Limited","Limited ", contentDf[,1]) 
  contentDf[,1] <- gsub("Limited  ","Limited ", contentDf[,1]) 
  contentDf[,1] <- gsub("Incorporated","Incorporated ", contentDf[,1]) 
  contentDf[,1] <- gsub("Incorporated  ","Incorporated ", contentDf[,1])
  colnames(contentDf)[1] <- "content"
  
  contentDf$PublicCompany <- NA
  contentDf$PublicTicker <- NA
  contentDf$PublicIndustry <- NA
  contentDf$Subsidiary <- NA
  contentDf$ParentCompany <- NA
  contentDf$ParentTicker <- NA
  contentDf$ParentIndustry <- NA

  #Load additional functions required
  assignCompany <- function(currentvalue,company,refers) {
    occurrences <- length(strsplit(tolower(contentDf$content[refers]),tolower(company),fixed=FALSE)[[1]])-1
    
    if (is.na(currentvalue)) 
    { 
      result <- sprintf("%s (%d)",company,occurrences) 
    } else
    { 
      result <- paste(currentvalue ,sprintf("%s (%d)",company,occurrences,sep=',')) 
    }
    result
  }

  assignC.ASXCode <- function(currentvalue,company,refers)
  {
    if (is.na(currentvalue)) 
    {
      result <- as.character(ASXdata[ASXdata$CompanyName==company,]$Ticker) 
    } else
    { 
      result <- paste(currentvalue ,as.character(ASXdata[ASXdata$CompanyName==company,]$Ticker),sep=',')
    }
    result
  }

  assignC.Industry <- function(currentvalue,company,refers)
  {
    if (is.na(currentvalue)) 
    {
      result <- as.character(ASXdata[ASXdata$CompanyName==company,]$Sector)
    } else
    { 
      result <- paste(currentvalue ,as.character(ASXdata[ASXdata$CompanyName==company,]$Sector),sep=',')
    }
    result
  }

  assignSubsidiary <- function(currentvalue,company,refers)
  {
    occurrences <- length(strsplit(tolower(contentDf$content[refers]),tolower(company),fixed=FALSE)[[1]])-1
    
    if (is.na(currentvalue)) 
    { 
      result <- sprintf("%s (%d)",company,occurrences) 
    } else
    { 
      result <- paste(currentvalue ,sprintf("%s (%d)",company,occurrences,sep=',')) 
    }
    result
  }

  for (company in ASXdata [,1])
  {
    refers <- grep(tolower(company),tolower(contentDf[,1]))
    
    if (length(refers) > 0) 
    { 
      for (refermember in refers)
      { 
        contentDf[refermember, ]$PublicCompany <- assignCompany(
          contentDf[refermember, ]$PublicCompany ,company,refermember)
        contentDf[refermember, ]$PublicTicker <- assignC.ASXCode(
          contentDf[refermember, ]$PublicTicker ,company,refermember)
        contentDf[refermember, ]$PublicIndustry <- assignC.Industry(
          contentDf[refermember, ]$PublicIndustry ,company,refermember) 
      } 
    }
  }

  for (company in SubsidiariesData$Company.Name)
  {
    refers <- grep(tolower(company),tolower(contentDf[,1]))
    
    if (length(refers) > 0) 
    { 
      for (refermember in refers)
      { 
        contentDf[refermember, ]$Subsidiary <- assignSubsidiary(
          contentDf[refermember, ]$Subsidiary ,company,refermember)
        Parent <- unique(SubsidiariesData$parentName[
          which(grepl(paste("^",company,"$",sep = ""),SubsidiariesData$Company.Name))])
        P.ASXCode <- unique(SubsidiariesData$parentTicker[
          which(grepl(paste("^",company,"$",sep = ""),SubsidiariesData$Company.Name))])
        P.Industry <- unique(SubsidiariesData$parentSector[
          which(grepl(paste("^",company,"$",sep = ""),SubsidiariesData$Company.Name))])
        contentDf[refermember, ]$ParentCompany <- gsub(
          "NA,","",(paste(contentDf[refermember, ]$ParentCompany,Parent,sep = ",")))
        contentDf[refermember, ]$ParentTicker <- gsub(
          "NA,","",(paste(contentDf[refermember, ]$ParentTicker,P.ASXCode,sep = ",")))
        contentDf[refermember, ]$ParentIndustry <- gsub(
          "NA,","",(paste(contentDf[refermember, ]$ParentIndustry,P.Industry,sep = ","))) 
      } 
    }
  }
  contentDf <- subset(contentDf,select = -content)
  contentDf
}

```

# Use Detect Entity Functions and compare results
```{r eval=TRUE}
ASICdf <- ASICdf[!is.na(ASICdf$Content),] #Remove rows where content is NA

# NLP method
aContent <- annotateNLP(ASICdf$Content[1])
#aContent <- annotateNLP(expandEntityName(ASICdf$Content[1]))
detectedEntityNLP <- merge(detectEntityNLP(aContent),ASICdf$Content[1])
for (i in 2:nrow(ASICdf)) {
  if (!is.na(ASICdf$Content[i]))
    aContent <- rbind(aContent,annotateNLP(ASICdf$Content[i]))
    #aContent <- rbind(aContent,annotateNLP(expandEntityName(ASICdf$Content[i])))
    detectedEntityNLP <- rbind(detectedEntityNLP,merge(detectEntityNLP(aContent[i,]),ASICdf$Content[i]))
}
colnames(detectedEntityNLP)[5] <- "Original Content"
write.csv(detectedEntityNLP,paste(dataDir,"DetectedEntity_NLP.csv",sep=""),row.names = FALSE)

# Basic method
for (i in 2:nrow(ASICdf)) {
  if (!is.na(ASICdf$Content[i]))
    detectedEntityKnown <- rbind(detectedEntityKnown,merge(detectEntityKnown(ASICdf$Content[i]),ASICdf$Content[i]))
}
colnames(detectedEntityKnown)[8] <- "Original Content"
write.csv(detectedEntityKnown,paste(dataDir,"DetectedEntity_Known.csv",sep=""),row.names = FALSE) 
```